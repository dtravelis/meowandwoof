﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseManager : MonoBehaviour
{
    private bool isPaused = false;
    [SerializeField] GameObject canvas;


    public void Continue()
    {
        canvas.SetActive(false);
        Time.timeScale = 1.0f;
        isPaused = false;
    }

    public void LoadMainMenu()
    {
        Time.timeScale = 1.0f;
        SceneManager.LoadScene("MainMenu");
    }


    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (!isPaused && Input.GetKeyDown(KeyCode.P))
        {
            ActivatePause();
        }
        else if (isPaused && Input.GetKeyDown(KeyCode.P))
        {
            Continue();
        }
    }

    void ActivatePause()
    {
        isPaused = true;

        canvas.SetActive(true);

        Time.timeScale = 0;
    }
}
