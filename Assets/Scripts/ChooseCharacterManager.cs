﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChooseCharacterManager : MonoBehaviour
{
    public void PulsaCat()
    {
        Debug.LogError("He pulsado Cat");

        SceneManager.LoadScene("Gameplay");
    }

    public void PulsaDog()
    {
        Debug.LogError("He pulsado Dog");

        SceneManager.LoadScene("Gameplay");
    }

    public void PulsaReturn()
    {
        Debug.LogError("He pulsado Return");

        SceneManager.LoadScene("MainMenu");
    }
}
