﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public void PulsaPlay()
    {
        Debug.LogError("He pulsado Play");

        SceneManager.LoadScene("ChooseCharacter");
    }

    public void PulsaCreditos()
    {
        Debug.LogError("He pulsado Creditos");

        SceneManager.LoadScene("Credits");
    }

    public void PulsaExit()
    {
        Application.Quit();
    }
}
