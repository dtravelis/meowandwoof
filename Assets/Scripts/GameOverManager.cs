﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour
{
    public void PulsaPlayAgain()
    {
        Debug.LogError("He pulsado PlayAgain");

        SceneManager.LoadScene("ChooseCharacter");
    }

    public void PulsaReturnToMenu()
    {
        Debug.LogError("He pulsado ReturnToMenu");

        SceneManager.LoadScene("MainMenu");
    }

    public void PulsaExit()
    {
        Application.Quit();
    }
}
