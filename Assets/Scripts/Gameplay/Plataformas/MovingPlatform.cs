﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{


    [SerializeField] float speed;

    [SerializeField] float maxY;
    float posOriginalY;
    float direccion = -2;
    public float limitePositivo;
    public float limiteNegativo;
    //[SerializeField] AudioSource audioA;
             

    private void Awake()
    {
       posOriginalY = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.y > limitePositivo)
        {
            direccion = direccion * -1;
        }
        else if (transform.position.y < limiteNegativo)
        {
            direccion = direccion * -1;
        }

        transform.Translate(0, direccion * speed * Time.deltaTime, 0);
        
    }


}
