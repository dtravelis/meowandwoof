﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cofre : MonoBehaviour
{
    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B))
        {
            collider.enabled = false;
            graphics.SetActive(false);
        }
    }
}
