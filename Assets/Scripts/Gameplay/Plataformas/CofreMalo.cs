﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CofreMalo : MonoBehaviour
{
    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            collider.enabled = false;
            graphics.SetActive(false);
        }
    }
}
