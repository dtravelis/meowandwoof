﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FishWeapon : Weapon
{

    public GameObject FishBullet;
    public GameObject BackFishBullet;

    public float cadencia;

    //public AudioSource audioSource;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate(FishBullet, this.transform.position, Quaternion.identity, null);

        //Play audio
        //audioSource.Play();
    }

    public override void BackShoot()
    {
        Instantiate(BackFishBullet, this.transform.position, Quaternion.identity, null);

        //Play audio
        //audioSource.Play();
    }


}
