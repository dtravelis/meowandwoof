﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
    [SerializeField] GameObject graphics;

    [SerializeField] BoxCollider2D collider;

    private float timeCounter;
    private float timeToShoot;

    private float timeShooting;

    private bool isShooting;

    //private ScoreManager sm;

    [SerializeField] private Animator anim;
    [SerializeField] GameObject bullet;

    [Header("Player Lives")]
    public GameObject BossLife_01;
    public GameObject BossLife_02;
    public GameObject BossLife_03;

    public int lives = 3;
    private bool dead = false;
    public float DeathLimit;



    private void Awake()
    {

        //sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();

        Inicitialization();
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (dead)
        {
            return;
        }

       

        if (transform.position.y < DeathLimit)
        {
            StartCoroutine(EnemyDeath());
        }


    }


    protected virtual void Inicitialization()
    {
        timeCounter = 0.0f;
        timeToShoot = 3.0f;
        timeShooting = 1.0f;
        isShooting = false;
    }

    protected virtual void EnemyBehaviour()
    {
        timeCounter += Time.deltaTime;

        if (timeCounter > timeToShoot)
        {
            if (!isShooting)
            {
                isShooting = true;
                Instantiate(bullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
            }
            if (timeCounter > (timeToShoot + timeShooting))
            {
                timeCounter = 0.0f;
                isShooting = false;
            }
        }

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        EnemyBehaviour();
}

    


    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "PlayerBullet")
        {

            anim.SetTrigger("dead");

            StartCoroutine(DestroyEnemy());
        }
        else if (other.tag == "Muro")
        {
            Destroy(this.gameObject);
        }
    }


    IEnumerator DestroyEnemy()
    {


        dead = true;


        lives--;

        BossLife_01.SetActive(false);
        

        yield return new WaitForSeconds(1.0f);

        if (lives > 0)
        {
            StartCoroutine(ReviveEnemy());
        }

        else if (lives <= 0)
        {
            BossLife_03.SetActive(false);
            yield return new WaitForSeconds(1.7f);

            Debug.LogError("Has ganado");

            SceneManager.LoadScene("Win");



        }
    }

    IEnumerator ReviveEnemy()
    {
        dead = false;

        if (lives == 2)
        {
            BossLife_01.SetActive(false);
        }
        else if (lives == 1)
        {
            BossLife_02.SetActive(false);
        }

        //graphic.SetActive(true);

        collider.enabled = true;

        //rb2d.isKinematic = true;

        yield return new WaitForSeconds(0.1f);


    }

    IEnumerator EnemyDeath()
    {


        dead = true;
        

        yield return new WaitForSeconds(1.0f);
        BossLife_01.SetActive(false);
        BossLife_02.SetActive(false);
        BossLife_03.SetActive(false);
        yield return new WaitForSeconds(1.7f);

        Debug.LogError("Has ganado");

        SceneManager.LoadScene("Win");

    }
}
