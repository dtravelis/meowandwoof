﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wolf : MonoBehaviour
{


    [SerializeField] float speed;

    [SerializeField] float maxX;
    float posOriginalX;
    float direccion = -2;
    public float limitePositivo;
    public float limiteNegativo;

    [SerializeField] GameObject graphics;
    [SerializeField] Collider2D collider;


    //private ScoreManager sm;

    private void Awake()
    {

        //sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();

        posOriginalX = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x > limitePositivo)
        {
            direccion = direccion * -1;
            graphics.transform.localScale = new Vector3(3, 3, 3);
        }
        else if (transform.position.x < limiteNegativo)
        {
            direccion = direccion * -1;
            graphics.transform.localScale = new Vector3(-3, 3, 3);
        }

        transform.Translate(direccion * speed * Time.deltaTime, 0, 0);


    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "PlayerBullet")
        {
            Destroy(this.gameObject);
        }
        if (other.tag == "Player")
        {
            StartCoroutine(DestroyPlayer());
        }

    }


    //IEnumerator DestroyEnemy()
    //{
    //Sumar puntos
    //sm.AddScore(50);

    //Desactivo el grafico
    //graphics.SetActive(false);

    //Elimino el BoxCollider2D
    //GetComponent<Collider>().enabled = false;

    //Lanzo sonido de explosion
    //audioSource.Play();

    //Me espero 1 segundo
    //yield return new WaitForSeconds(1.0f);


    //}

    IEnumerator DestroyPlayer()
    {
        collider.enabled = false;

        //Me espero 1 segundo
        yield return new WaitForSeconds(3.0f);
        collider.enabled = true;


    }





    /*private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "MuroS_01")
        {
            direccion = direccion * -1;
            graphics.transform.localScale = new Vector3(-3, 3, 3);
        }else if (other.tag == "MuroS_02")
        {
            direccion = direccion * -1;
            graphics.transform.localScale = new Vector3(3, 3, 3);
        }
    }*/
}
