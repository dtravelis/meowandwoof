﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // Variables publicas para poder modificar desde el inspector
    public float maxS = 4f;
    public float maxSShift = 11f;
    //Variables privadas
    private Rigidbody2D rb2d = null;
    public Rigidbody rb;
    private float move = 0f;
    [SerializeField] private Animator anim;
    [SerializeField] private Transform graphics;
    [SerializeField] GameObject graphic;
    [SerializeField] Collider2D collider;
    [SerializeField] AudioSource audioL;
    [SerializeField] AudioSource audioAt;

    private bool flipped = false;

    private bool jump = false;
    private bool grounded = false;
    private bool run = false;

    private float speed = 0;
    
    [Header("Player Lives")]
    public GameObject Life_01;
    public GameObject Life_02;
    public GameObject Life_03;
    public GameObject Life_04;

    public int lives = 3;
    private bool dead = false;
    public float DeathLimit;

    [Header("Weapon")]
    private float shootTime = 0;

    public Weapon weapon;

    // Use this for initialization
    void Awake()
    {
        // Obtenemos el rigidbody y lo guardamos en la variable rb2d
        // para poder utilizarla más cómodamente
        rb2d = GetComponent<Rigidbody2D>();



    }

    /*public void Start()
    {

        PlayerLife = GameObject.Find("Lifes");


    }*/

    // Update is called once per frame
    void FixedUpdate()
    {

        if (dead)
        {
            return;
        }

        shootTime += Time.deltaTime;

        //Miramos el input Horizontal
        move = Input.GetAxis("Horizontal");

        jump = Input.GetButtonDown("Jump");
        run = Input.GetButton("Run");

        if (run)
        {
            speed = maxSShift;
        }
        else
        {
            speed = maxS;
        }

        if (jump && grounded)
        {
            rb2d.velocity = new Vector2(move * speed, rb2d.velocity.y + speed);
        }
        else
        {
            rb2d.velocity = new Vector2(move * speed, rb2d.velocity.y);
        }

        //Ponemos la velocidad horizontal y vertical
        anim.SetFloat("velocityH", Mathf.Abs(rb2d.velocity.x));
        anim.SetFloat("velocityV", Mathf.Abs(rb2d.velocity.y));
        anim.SetBool("grounded", grounded);

        if (rb2d.velocity.x < 0)
        {
            graphics.transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            graphics.transform.localScale = new Vector3(1, 1, 1);
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            Shoot();
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            BackShoot();
        }

        if (transform.position.y < DeathLimit)
        {
            StartCoroutine(PlayerDeath());
        }


    }

    public void Shoot()
    {
        if (shootTime > weapon.GetCadencia())
        {
            shootTime = 0f;
            weapon.Shoot();
            audioAt.Play();
        }
    }

    public void BackShoot()
    {
        if (shootTime > weapon.GetCadencia())
        {
            shootTime = 0f;
            weapon.BackShoot();
            audioAt.Play();
        }
    }



    private void OnCollisionEnter2D(Collision2D other)
    {
        grounded = true;
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        grounded = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy" || other.tag == "Spikes")
        {
            anim.SetTrigger("dead");
            StartCoroutine(DestroyPlayer());
        }else if (other.tag == "ExtraLife")
        {
            lives++;
            if (lives == 2)
            {                
                Life_02.SetActive(true);
            }
            else if (lives == 3)
            {
                Life_01.SetActive(true);
            }
            else if (lives == 4)
            {
                Life_04.SetActive(true);
            }
        }
        else if (other.tag == "BadHeart")
        {
            lives--;
            if (lives == 2)
            {
                Life_01.SetActive(false);
            }
            else if (lives == 3)
            {
                Life_04.SetActive(false);
            }
            else if (lives == 1)
            {
                Life_02.SetActive(false);
            }
            else if (lives <= 0)
            {
                Life_03.SetActive(false);
                
                Debug.LogError("Has muerto");

                SceneManager.LoadScene("GameOver");
                               
            }
        }

    }


    IEnumerator DestroyPlayer()
    {


        dead = true;


        lives--;

        Life_01.SetActive(false);

        speed = 0;

        //graphic.SetActive(false);


        //rb2d.isKinematic = false;

        audioL.Play();

        yield return new WaitForSeconds(1.0f);

        if (lives > 0)
        {
            StartCoroutine(RevivePlayer());
        }

        else if (lives <= 0)
        {
            Life_03.SetActive(false);
            yield return new WaitForSeconds(1.7f);

            Debug.LogError("Has muerto");

            SceneManager.LoadScene("GameOver");



        }
    }

        IEnumerator RevivePlayer()
        {
            dead = false;

            if (lives == 2)
            {
                Life_01.SetActive(false);
            }
            else if (lives == 1)
            {
                Life_02.SetActive(false);
            }

            //graphic.SetActive(true);

            collider.enabled = true;

            //rb2d.isKinematic = true;

            yield return new WaitForSeconds(0.1f);
            speed = maxS;


        }

        IEnumerator PlayerDeath()
        {


            dead = true;

            audioL.Play();

            yield return new WaitForSeconds(1.0f);
                Life_01.SetActive(false);
                Life_02.SetActive(false);
                Life_03.SetActive(false);
                yield return new WaitForSeconds(1.7f);

                Debug.LogError("Has muerto");

                SceneManager.LoadScene("GameOver");

        }
    
}
